# gulp笔记

[gulp 简体中文文档](https://github.com/lisposter/gulp-docs-zh-cn)

## gulpfile.js

### 1. 创建一个gulp任务

下面的实例创建一个名为 `uglify` 的 gulp 任务。

```js
var gulp = require('gulp'),
	uglify = require('uglify'),
	concat = require('concat');

gulp.task('uglify', function() {
	//1. 找到所有文件
	gulp.src(['datas/*.js'])
        //2. 压缩文件
		.pipe(uglify())
        //3. 合并成一个文件
		.pipe(concat('datas.js'))
        //4. 保存到指定目录
		.pipe(gulp.dest('dest/datas/'));
});

//合并以上指令，使用'default'参数就设置成gulp默认任务
gulp.task('default', ['clean'], function() {
	gulp.start('uglify');
});
```


### 2. 分析一个 gulpfile.js

参考 yeoman 构建 webapp 的一个gulpfile.js ，主要使用 bootstrap-sass + gulp 

基本任务如下：

- styles 将sass编译为css
- scripts 将ES6转为ES5
- lint js检查
- images 压缩&缓存图片
- fonts 设置字体
- extras 发布到dist
- clean 清除
- wiredep 引入依赖

基本任务很少单独用到，调试（或发布）时都是把基本任务串起来，用以构建项目。  
如下定义了serve调试（build构建）任务：  

serve 调试，类似debug  
　　┗ clean 清除  
　　┗ wiredep 引入依赖  
　　┗ styles 将sass编译为css  
　　┗ scripts 将ES6转为ES5  
　　┗ fonts 设置字体  
　　┗ watch 监听变化  


build 打包构建到dist  
　　┗ clean 清除  
　　┗ wiredep 引入依赖  
　　┗ lint js检查  
　　┗ html 压缩css、js、html  
　　　　┗ styles 将sass编译为css  
　　　　┗ scripts 将ES6转为ES5  
　　┗ images 压缩&缓存图片  
　　┗ fonts 设置字体  
　　┗ extras 发布到dist  



## 常用模块

- del 清除上次部署的文件  
- copy 复制文件  
- gulp-uglify 压缩js脚本  
- gulp-cssnano 压缩样式(https://www.npmjs.com/package/gulp-cssnano)  
- browser-sync CSS文件重载和浏览器同步(http://www.browsersync.cn/docs/)  
- wiredep 向html注入bower组件  
- gulp-sass 编译sass文件为css格式(https://www.npmjs.com/package/gulp-sass)  
- gulp-sourcemaps 浏览器调试时能够找到对应sass文件(https://www.npmjs.com/package/gulp-sourcemaps)  
- gulp-autoprefixer 为CSS添加供应商前缀，为旧浏览器生成代码(https://www.npmjs.com/package/gulp-autoprefixer)  
- main-bower-files (https://www.npmjs.com/package/main-bower-files)  
- gulp-useref 把html里零碎的引入合并成一个文件，识别build开头的注释(https://www.npmjs.com/package/gulp-useref)  
- gulp-print 打印出stream里面的所有文件名(https://juejin.im/entry/55c8dbb160b22a3ebdf34d57)  
- run-sequence 将一串task按顺序执行(https://www.npmjs.com/package/run-sequence)  
- gulp-load-plugins 插件管理模块(https://www.npmjs.com/package/gulp-load-plugins)  
- gulp-if 判断(https://www.npmjs.com/package/gulp-if)  
- gulp-htmlmin HTML文件压缩(https://www.npmjs.com/package/gulp-htmlmin)  
- gulp-size 显示文件大小(https://github.com/sindresorhus/gulp-size)  

==========================================

- gulp-concat 合并js脚本
- gulp-imagemin 压缩图片(https://www.npmjs.com/package/gulp-imagemin)
- gulp-html-replace 替换(https://www.npmjs.com/package/gulp-html-replace)
- gulp-babel 把 ES2016 的语法转译成 ES5
- gulp-cache 图片缓存(https://github.com/jgable/gulp-cache)
- gulp-eslint 检查js的语法错误(http://eslint.cn/docs/user-guide/configuring)。同类型的有：jslint,jshint
- gulp-filter 把stream里的文件根据一定的规则进行筛选过滤，有个restore()方法
- gulp-plumber 流式任务体系中报错用的(https://www.npmjs.com/package/gulp-plumber)
- gulp-bump 升级版本时升级依赖
- gulp-header 在压缩后的JS、CSS文件中添加头部注释








js测试

- chai 断言库
- mocha 测试框架



怎么使用？

[Chai.js断言库API中文文档](http://www.jianshu.com/p/f200a75a15d2)
[测试框架 Mocha 实例教程](http://www.ruanyifeng.com/blog/2015/12/a-mocha-tutorial-of-examples.html)




