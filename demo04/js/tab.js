;(function($) { 

    $.fn.tab = function(options) {
        
        //定义默认
        $.fn.tab.defaults = {
            trigger_event_type: 'click',
        };

        // 将defaults 和 options 参数合并到{}
        var opts = $.extend({}, $.fn.tab.defaults, options);
        console.log(opts.trigger_event_type);

        return this.each(function() {
            var obj = $(this);

            obj.find('.tab_header li').on(opts.trigger_event_type, function(event) {
            	obj.find('.tab_header li').removeClass('active');
            	$(this).addClass('active');

            	obj.find('.tab_content .tab_item').removeClass('active');
            	obj.find('.tab_content .tab_item').eq($(this).index()).addClass('active');

            })                
        });
    }

})(jQuery);

$(function() {
    $('#tab').tab();
});