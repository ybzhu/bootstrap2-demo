# gulp的综合运用


## 一个例子

demo04 目的是综合运用 gulp 构建项目。gulp 完成以下几个任务：

demo04 browserSync发布和watch监视
　　┗ demo04-clean：清理上次产生的目录及文件
　　┗ demo04-wiredep：引入bower管理的依赖
　　┗ js 语法检查（略）
　　┗ demo04-html：合并零碎文件，压缩css、js和html
　　　　┗ demo04-scss：scss 转换
　　　　┗ ES6 转换（略）
　　┗ 压缩图片（略）
　　┗ 复制字体文件（略）
　　┗ demo04-extras：其他


### gulp-htmlmin 的用法

由于 demo04 项目比较小，很多任务都省略了。
重点看一下 demo04-html 任务：

```javascript
// 压缩 html 文件中引用的 css 样式、 js 脚本和 html 代码
gulp.task('demo04-html', ['demo04-sass'], function() {
    return gulp.src('demo04/*.html')
        // 这里使用searchPath设置搜索目录是要对应 html 中的 <link> 和 <script>
        .pipe($.useref({searchPath: ['demo04', '.']}))
        // 压缩以<script src=""></script>标签引入的 js 脚本
        .pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
        // 压缩以<link rel="stylesheet" href="">标签引入的 css 样式
        .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
        //压缩 html 文件
        .pipe($.if(/\.html$/, $.htmlmin({
            // htmlmin 的一些常用参数
            // 去除空格
            collapseWhitespace: true,
            // 压缩直接写在html中的css
            minifyCSS: true,
            // 压缩直接写在html中的js
            minifyJS: {compress: {drop_console: true}},
            // 用minifier规则去掉注释
            processConditionalComments: true,
            // 去掉所有注释
            removeComments: true,
            // 删除值为空白的属性
            removeEmptyAttributes: true,
            // 移除script标签中的 type="text/javascript"
            removeScriptTypeAttributes: true,
            // 移除link标签中的 type="text/css"
            removeStyleLinkTypeAttributes: true
        })))
        .pipe(gulp.dest('dist'));
});
```


更多 htmlmin 的 `参数` 请[参考html-minifier](https://github.com/kangax/html-minifier)
这个任务还用到了 `gulp-useref` 、 `gulp-cssnano` 和 `gulp-uglify` ，不展开了。


### run-sequence 的用法

gulp 有 nodejs 异步的特点，所以在需要按顺序执行任务时就要用到 run-sequence 。

```javascript
// 注意使用 runSequence 限定执行任务的顺序为：
// * demo04-clean 和 demo04-wiredep
// * demo04-html 和 demo04-extras
// * 回调函数
gulp.task('demo04', function() {
    runSequence(['demo04-clean', 'demo04-wiredep'], ['demo04-html', 'demo04-extras'], function() {
        browserSync.init({
            notify: false,
            port: 9000,
            server: {
                baseDir: ['dist']
            }
        });

        gulp.watch(['demo04/*.html', 'demo04/img/**/*']).on('change', reload);
        gulp.watch('demo04/scss/*.scss', ['demo04-sass']);
        gulp.watch('bower.js', ['demo04-wiredep']);
    });
});
```


## 拓展

1. [gulp-size](https://www.npmjs.com/package/gulp-size)显示构建完成的文件大小
2. scss语法检查模块


## 参考

- [gulp-htmlmin](https://www.npmjs.com/package/gulp-htmlmin)  
- [html-minifier](https://github.com/kangax/html-minifier)  
- [run-sequence](https://www.npmjs.com/package/run-sequence)  

