const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const runSequence = require('run-sequence');
const wiredep = require('wiredep').stream;
const imageDataURI = require('gulp-image-data-uri');

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

var dev = true;

// demo01 wiredep 插件的范例
gulp.task('demo01', function() {
  gulp.src('demo01/index.html')
    .pipe(wiredep({
    	ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('demo01'));
});

// demo01 拓展 main-bower-files 和 gulp-print 插件的范例
gulp.task('demo01-print', function() {
	gulp.src(require('main-bower-files')())
	.pipe($.print(function(filepath) {
		return "built: " + filepath;
	}));
});

// demo02 sass 插件范例
gulp.task('demo02-sass', function() {
	del.bind(null, ['demo02/css']);
	return gulp.src('demo02/scss/*.scss')
		.pipe($.sass({
			/* 
			 * sass 输出为 css 的风格有4种:
			 * nested		大括号不会换行
			 * expanded		大括号换行
			 * compact		单行的css
			 * compressed	压缩的css
			*/
			outputStyle: 'compressed'
		}))
		.pipe(gulp.dest('demo02/css'))
		.pipe(reload({stream: true}));
});

// demo02 browser-sync 插件范例
gulp.task('demo02', ['demo02-sass'], function() {
	browserSync.init({
		notify: false,
		port: 9000,
		server: {
		  baseDir: 'demo02'
		}
	});

	gulp.watch('demo02/scss/*.scss', ['demo02-sass']);
	gulp.watch('demo02/*.html').on('change', reload);
});

// demo03 使用 sourcemaps 插件，并内嵌自动添加供应商前缀
gulp.task('demo03-sourcemaps', function() {
	del.bind(null, ['demo03/css']);
	return gulp.src('demo03/scss/*.scss')
		// sourcemaps 开始
		.pipe($.sourcemaps.init())
		.pipe($.sass({
			outputStyle: 'expanded'
		}))
		// 紧接着 scss 的编译结果
		.pipe($.autoprefixer({
			// 这个配置选项说明要支持那些浏览器
			browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
		}))
		// sourcemaps 结束。包裹编译 scss 的语句就能形成映射
		.pipe($.sourcemaps.write())
		.pipe(gulp.dest('demo03/css'))
		.pipe(reload({stream: true}));
});

// demo03 和 demo02 一样，可以在浏览器中监听样式变化、实时调试
gulp.task('demo03', ['demo03-sourcemaps'], function() {
	browserSync.init({
		notify: false,
		port: 9000,
		server: {
		  baseDir: 'demo03'
		}
	});

	gulp.watch('demo03/scss/*.scss', ['demo03-sourcemaps']);
	gulp.watch('demo03/*.html').on('change', reload);
});

// demo04 是一个综合例子，编译 scss 是一个基本任务
gulp.task('demo04-sass', function() {
	del.bind(null, ['demo04/css']);
	return gulp.src('demo04/scss/*.scss')
		// plumber是显示错误的
		.pipe($.plumber())
		.pipe($.sass({
			outputStyle: 'expanded'
		}))
		.pipe($.autoprefixer({
			browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
		}))
		.pipe(gulp.dest('demo04/css'))
		.pipe(reload({stream: true}));
});

// 压缩 html 文件中引用的 css 样式、 js 脚本和 html 代码
gulp.task('demo04-html', ['demo04-sass'], function() {
	return gulp.src('demo04/*.html')
		// 这里使用searchPath设置搜索目录是要对应 html 中的 <link> 和 <script>
		.pipe($.useref({searchPath: ['demo04', '.']}))
		// 压缩以<script src=""></script>标签引入的 js 脚本
		.pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
		// 压缩以<link rel="stylesheet" href="">标签引入的 css 样式
		.pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
		//压缩 html 文件
		.pipe($.if(/\.html$/, $.htmlmin({
			// htmlmin 的一些常用参数
			// 去除空格
			collapseWhitespace: true,
			// 压缩直接写在html中的css
			minifyCSS: true,
			// 压缩直接写在html中的js
			minifyJS: {compress: {drop_console: true}},
			// 用minifier规则去掉注释
			processConditionalComments: true,
			// 去掉所有注释
			removeComments: true,
			// 删除值为空白的属性
			removeEmptyAttributes: true,
			// 移除script标签中的 type="text/javascript"
			removeScriptTypeAttributes: true,
			// 移除link标签中的 type="text/css"
			removeStyleLinkTypeAttributes: true
		})))
		.pipe(gulp.dest('dist'));
});

// 定义 dist 目录作为 demo04 输出目录
gulp.task('demo04-clean', del.bind(null, ['dist', 'demo04/css']));

// 引入 bower 管理的依赖
gulp.task('demo04-wiredep', function() {
	gulp.src('demo04/*.html')
	    .pipe(wiredep({
	    	ignorePath: /^(\.\.\/)+/
	    }))
	    .pipe(gulp.dest('demo04'));
});

// 把 demo04 目录下的静态文件放到 dist 中
gulp.task('demo04-extras', function() {
	return gulp.src([
		'demo04/*',
		// 排除 html 文件，因为 demo04-html 任务会把压缩好的 html 输出到 dist 目录
		'!demo04/*.html',
		// 排除 scss 文件，因为实际用到的是 css 文件
		'!demo04/scss'
		], {
		dot: true
		}).pipe(gulp.dest('dist'));
});

// demo04 的拓展，显示构建好的文件大小（压缩好的）
gulp.task('demo04-size', function() {
	return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
});

// 注意使用 runSequence 限定执行任务的顺序为：
// * demo04-clean 和 demo04-wiredep
// * demo04-html 和 demo04-extras
// * 回调函数
gulp.task('demo04', function() {
	runSequence(['demo04-clean', 'demo04-wiredep'], ['demo04-html', 'demo04-extras'], ['demo04-size'], function() {
		browserSync.init({
			notify: false,
			port: 9000,
			server: {
				baseDir: ['dist']
			}
		});

		gulp.watch(['demo04/*.html', 'demo04/img/**/*']).on('change', reload);
		gulp.watch('demo04/scss/*.scss', ['demo04-sass']);
		gulp.watch('bower.js', ['demo04-wiredep']);
	});
});

// 跨标签读取 sessionstorage 的demo
gulp.task('storage01', function() {
	browserSync.init({
		notify: false,
		port: 9000,
		server: {
		  baseDir: 'storage01'
		}
	});

	gulp.watch('storage01/*.html').on('change', reload);
})

// 压缩图片
gulp.task('yasuo', function() {
	del('imagemin/dist/*.{png,jpg,jpeg,gif}')
	.then(paths => {
		console.log('delete dist');
	});
	// gulp.src('imagemin/source/*.{png,jpg,gif}')
	// gulp.src('imagemin/source/subject-detail-*.gif')
	gulp.src('imagemin/source/subject-index-*')
	.pipe($.plumber())
	.pipe($.imagemin([
	    $.imagemin.gifsicle({interlaced: true, optimizationLevel: 3}),
	    $.imagemin.jpegtran({progressive: true}),
	    $.imagemin.optipng({optimizationLevel: 7})
	]))
	.pipe(gulp.dest('imagemin/dist'));
});

// 将图片转成 base64
gulp.task('base64', function() {
	del('imagemin/base64/*')
	.then(paths => {
		console.log('delete base64');
	});
	gulp.src('imagemin/source/subject-index-basic-05.jpg')
	.pipe(imageDataURI())
	.pipe(gulp.dest('imagemin/base64'));
});
