# wiredep

wiredep插件可以向 html、jade、less、sass、scss、styl、yaml 格式的文件中注入 bower 组件。


## 一个例子

以 html 页面为例，如果该页面需要 bootstrap 和 jquery ，那么怎样自动地将 bower 下载的组件添加到页面的 link 、script 标签中呢？

wiredep 通过匹配注释，向 html 文件指定的位置添加引用。

注释的模板如 index.html 所示，在指定为位置会有如下的注释：

```html
<!-- bower:css -->
<!-- endbower -->

<!-- bower:js -->
<!-- endbower -->
```

运行如下 gulp 指令

```shell
cd bootstrap2-demo
gulp demo01
```


看看 index.html 是不是插入了 bower 组件，暂时只用 bower 引入了 bootstrap 和 jquery 组件：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <!-- bower:css -->
    <link rel="stylesheet" href="bower_components/bootstrap/docs/assets/css/bootstrap.css" />
    <!-- endbower -->
</head>
<body>
    <h1>hello world!</h1>
    <!-- bower:js -->
    <script src="bower_components/jquery/jquery.js"></script>
    <script src="bower_components/bootstrap/docs/assets/js/bootstrap.js"></script>
    <!-- endbower -->
</body>
</html>
```


## 分析

gulp 命令执行的任务在 gulpfile.js 文件中；
引用的路径来自每一个组件的 bower.js 文件 main 的值；
如果要自定义 main 的值，可以在自己项目的 bower.js 文件中重写 main ，如下

```json
{
  ...
  "dependencies": {
    "package-without-main": "1.0.0"
  },
  "overrides": {
    "package-without-main": {
      "main": [
        "dist/package-without-main.css", 
        "dist/package-without-main.js"
      ]
    }
  }
}
```


## 拓展

1. [gulp-useref](https://www.npmjs.com/package/gulp-useref)插件通过识别 html 文件中的注释，将注释内的文件合并成一个文件，如下的注释将合并为 combined.js 文件

```html
<!-- build:js scripts/combined.js -->
    <!-- 多个js文件的引入 -->
<!-- endbuild -->
```


2. [main-bower-files](https://www.npmjs.com/package/main-bower-files) 插件也是根据 bower.js 文件中的 main 属性读取文件，只是不做依赖注入的步骤


3. [gulp-print](https://juejin.im/entry/55c8dbb160b22a3ebdf34d57)插件可以打印出stream里面的所有文件名。

在 gulpfile.js 中定义如下任务，同时用到了 main-bower-files：

```javascript
// demo01 拓展 main-bower-files 和 gulp-print 插件的范例
gulp.task('demo01-print', function() {
  gulp.src(require('main-bower-files')())
  .pipe($.print(function(filepath) {
    return "built: " + filepath;
  }));
});
```

`注意`：我在 powershell 终端里竟然打印不出来，但是在 git-bash 和 cmd 中都可以打印出来


## 参考

- [wiredep](https://www.npmjs.com/package/wiredep)
- [常用gulp插件介绍（一）](https://juejin.im/entry/55c8dbb160b22a3ebdf34d57)
- [gulp-useref](https://www.npmjs.com/package/gulp-useref)
- [main-bower-files](https://www.npmjs.com/package/main-bower-files)
- [gulp-print](https://juejin.im/entry/55c8dbb160b22a3ebdf34d57)