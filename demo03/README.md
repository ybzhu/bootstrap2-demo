# sourcemaps & autoprefixer


## 一个例子

这个例子来自 demo02 的例子，完善了以下几个功能：

1. 使用 autoprefixer 插件自动为 css 样式添加供应商前缀
2. 本例完成 demo02 中追踪 scss 样式的任务

autoprefixer 主要做两件事情：

1. 确定 css 样式要支持哪些浏览器（范围）
2. 根据范围为 css 样式添加供应商前缀，同时会删掉多余的 css 样式

既然是对 css 样式的构建，那么自然紧接着 scss 的编译结果

剩下的是使用 sourcemaps 构建 scss 和 css 的映射关系。

只要在自定义 gulp 构建任务时，用 sourcemaps.init() 和 sourcemaps.write() 包裹编译 scss 的语句（管道）。gulpfile.js 中自定义任务如下：

```javascript
// demo03 使用 sourcemaps 插件，并内嵌自动添加供应商前缀
gulp.task('demo03-sourcemaps', function() {
  del.bind(null, ['demo03/css']);
  return gulp.src('demo03/scss/*.scss')
    // sourcemaps 开始
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      outputStyle: 'expanded'
    }))
    // 紧接着 scss 的编译结果
    .pipe($.autoprefixer({
      // 这个配置选项说明要支持那些浏览器
      browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
    }))
    // sourcemaps 结束。包裹编译 scss 的语句就能形成映射
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('demo03/css'))
    .pipe(reload({stream: true}));
});
```

## 验证

现在和 demo02 对比看看，demo03 在浏览器审查元素是宗追的到是不是 scss 样式～。对比图如下：

![debuug in chrome from deamo02](./img/no_sourcemaps.png)

在 demo03 中， h1 标签多加了一个渐变的样式，带供应商前缀的（圈出）就是 autoprefixer 帮助构建的

![debuug in chrome from deamo03](./img/use_sourcemaps_and_autoprefixer.png)


## 参考

- [gulp-sourcemaps](https://www.npmjs.com/package/gulp-sourcemaps)
- [sourcemaps支持的插件列表](https://github.com/floridoo/gulp-sourcemaps/wiki/Plugins-with-gulp-sourcemaps-support)
- [gulp-autoprefixer](https://www.npmjs.com/package/gulp-autoprefixer)
- [autoprefixer配置选项列表](https://github.com/postcss/autoprefixer#options)
- [browserslist](http://browserl.ist/)
