# 简介

## 项目结构

这个仓库放了很多杂七杂八的例子

- app ———— 一个 bootstrap2 的例子
- demo01 ———— npm 插件 wiredep 的范例 和 2个类似的插件
- demo02 ———— browser-sync 和 gulp-sass 插件结合的一个范例
- demo03 ———— gulp-sourcemaps 和 gulp-autoprefixer 插件结合的一个范例
- demo04 ———— 综合例子（包括编译 scss ，自动引入 bower 依赖，压缩代码等）
- storage01 ———— 跨标签读取 sessionstorage 的demo
- imagemin ———— 压缩图片
- icons ———— Material icons 预览

