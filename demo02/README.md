# browser-sync & gulp-sass

Browsersync 能让浏览器实时、快速响应您的文件更改（html、js、css、sass、less等）并自动刷新页面

## 一个例子

以 scss 样式为例，要自动化构建以下几个步骤：

1. 将 scss 文件编译为 css 文件；
2. 更改 scss 文件中的样式，能够实时的在浏览器中刷新（重新载入）；
3. 用浏览器调试的时候能够追踪到 scss 文件的中样式，而不是 css 文件中的样式；（本 demo 中没有涉及，留给下一个 demo）

在 demo02 中，index.html 页面引入的 css/main.css 样式文件并没有直接提供，而是由编写 scss/main.scss 而来。

在终端执行 `gulp demo02-sass` 命令可以将 scss 样式编译成 css ，实现过程请参考 gulpfile.js 文件

```js
// demo02 sass 插件范例
gulp.task('demo02-sass', function() {
    // 删除 css 目录下已有的样式文件
    del.bind(null, ['demo02/css']);
    return gulp.src('demo02/scss/*.scss')
    .pipe($.sass({
        /* 
         * sass 输出为 css 的风格有4种:
         * nested       大括号不会换行
         * expanded     大括号换行
         * compact      单行的css
         * compressed   压缩的css
        */
        outputStyle: 'compressed'
    }))
    .pipe(gulp.dest('demo02/css'))
    .pipe(reload({stream: true}));
});
```

在终端执行 `gulp demo02` 命令可以将 demo02 发布到 browser-sync 创建的服务器中，gulp 的构建命令如下：

```js
// demo02 browser-sync 插件范例
gulp.task('demo02', ['demo02-sass'], function() {
    // 构建一个服务器，端口为9000，根目录为demo02
    browserSync.init({
        notify: false,
        port: 9000,
        server: {
          baseDir: 'demo02'
        }
    });

    // 监听变化
    // scss 有变化就重新编译为 css
    gulp.watch('demo02/scss/*.scss', ['demo02-sass']);
    // html 页面有变化就刷新页面
    gulp.watch('demo02/*.html').on('change', reload);
});
```

默认浏览器会打开 http://localhost:9000/ ，请试试更改 scss 样式，是不是在浏览器中能实时看到新样式呢～


## 参考

- [Browsersync + Gulp.js](http://www.browsersync.cn/docs/gulp/)
- [Browsersync options (选项配置)](http://www.browsersync.cn/docs/options/)
- [gulp-sass](https://www.npmjs.com/package/gulp-sass)

